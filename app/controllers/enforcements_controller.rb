class EnforcementsController < ApplicationController
  before_action :authenticate_admin!, only: [:destroy]
  before_action :set_enforcement, only: [:update, :destroy]
  before_action :set_enforcement_by_token, only: [:edit, :denied]

  # GET /enforcements/1/edit
  def edit
    @vote = Vote.new
    if @enforcement.voted
      redirect_to action: "denied", status: 401, token: params[:token]
    end
  end

  def denied
  end

  # PATCH/PUT /enforcements/1
  # PATCH/PUT /enforcements/1.json
  def update
    if @enforcement.survey.grade.nil?
      @enforcement.survey.grade = 0
    end

    @vote = Vote.new(grade: params[:enforcement][:grade].to_f)

    @enforcement.survey.grade += @vote.grade

    respond_to do |format|
      if @enforcement.voted
        redirect_to action: "denied", status: 401, token: @enforcement.token
      end

      if @vote.valid? && @enforcement.update(voted: true) && @enforcement.survey.save
        format.html { redirect_to enforcements_denied_path(token: @enforcement.token) }
        format.json { render :show, status: :ok, location: @enforcement }
      else
        @vote.errors.each do |attribute, message|
          @enforcement.errors[attribute] << message
          @enforcement.errors[attribute].uniq!
        end
        format.html { render :edit }
        format.json { render json: @enforcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /surveys/1
  # DELETE /surveys/1.json
  def destroy
    @enforcement.destroy
    respond_to do |format|
      format.html { redirect_to surveys_url, notice: "Pesquisa deletada com sucesso" }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_enforcement_by_token
    @enforcement = Enforcement.find_by(token: params[:token])
    @survey = @enforcement.survey
  end

  def set_enforcement
    @enforcement = Enforcement.find(params[:id])
    @survey = @enforcement.survey
  end
end
