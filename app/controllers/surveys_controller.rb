class SurveysController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_survey, only: [:show, :edit, :update, :destroy, :send_mail, :add_person]
  before_action :check_if_survey_is_closed, only: [:edit, :update]

  # GET /surveys
  # GET /surveys.json
  def index
    @surveys = Survey.all
  end

  # GET /surveys/1
  # GET /surveys/1.json
  def show
  end

  # GET /surveys/new
  def new
    @survey = Survey.new
  end

  # GET /surveys/1/edit
  def edit
  end

  # POST /surveys
  # POST /surveys.json
  def create
    @people_attrs = survey_params[:people]
    @people_attrs.delete_if { |email| email.empty? || email.blank? }
    @survey = Survey.new(survey_params.except(:people))

    respond_to do |format|
      if @survey.save
        @people_attrs.each do |person|
          person = Person.find(person.to_i)
          enforcement = @survey.enforcements.create(person_id: person.id)

          if !enforcement.save
            @survey.errors.messages[:email].append("#{person.email} já está sendo utilizado para esta pesquisa")

            format.html { render :edit }
          end
        end

        format.html { redirect_to @survey, notice: "Pesquisa criada com sucesso" }
        format.json { render :show, status: :created, location: @survey }
      else
        format.html { render :new }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_person
    person_id = params[:person_id]

    person = Person.find(person_id.to_i)
    enforcement = @survey.enforcements.create(person_id: person.id)

    respond_to do |format|
      unless enforcement.save
        @survey.errors.messages[:email].append("#{person.email} já está sendo utilizado para esta pesquisa")

        format.json { render json: @survey.errors, status: :unprocessable_entity }
        format.html { render :edit }
      else
        format.json { render json: { enforcement_id: enforcement.id, person: person } }
      end
    end
  end

  # PATCH/PUT /surveys/1
  # PATCH/PUT /surveys/1.json
  def update
    # @people_attrs = survey_params[:people]
    # @people_attrs.delete_if { |email| email.empty? || email.blank? }

    respond_to do |format|
      if @survey.update(survey_params.except(:people))
        format.html { redirect_to @survey, notice: "Pesquisa editada com sucesso" }
        format.json { render :show, status: :ok, location: @survey }
      else
        format.html { render :edit }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /surveys/1
  # DELETE /surveys/1.json
  def destroy
    @survey.destroy
    respond_to do |format|
      format.html { redirect_to surveys_url, notice: "Pesquisa deletada com sucesso" }
      format.json { head :no_content }
    end
  end

  def send_mail
    @survey.send_request_mails

    respond_to do |format|
      format.html { redirect_to @survey, notice: "Emails enviados com sucesso" }
      format.json { render :show, status: :ok, location: @survey }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_survey
    @survey = Survey.find(params[:id])
    @people = @survey.people
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def survey_params
    params.require(:survey).permit(
      :question,
      people: [],
    )
  end

  def check_if_survey_is_closed
    if @survey.closed
      redirect_to @survey, notice: "Não é possível editar uma pesquisa fechada"
    end
  end
end
