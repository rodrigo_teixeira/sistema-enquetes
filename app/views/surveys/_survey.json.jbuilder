json.extract! survey, :id, :grade, :question, :created_at, :updated_at
json.url survey_url(survey, format: :json)
