json.extract! person, :id, :email, :name
json.url person_url(person, format: :json)
