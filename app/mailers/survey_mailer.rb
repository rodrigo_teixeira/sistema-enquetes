class SurveyMailer < ApplicationMailer
  helper :surveys

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.survey_mailer.send.subject
  #
  default from: "rh@happyel.com.br"

  def send_request
    @enforcement = params[:enforcement]
    @person = params[:enforcement].person
    @survey = params[:enforcement].survey

    mail to: @person.email, subject: "Pesquisa de satisfação HappyHel"
  end

  def send_result
    @person = params[:enforcement].person
    @survey = params[:enforcement].survey
    mail to: @person.email, subject: "Pesquisa de satisfação HappyHel"
  end
end
