module SurveysHelper
  def survey_count(survey_id)
    Survey.find(survey_id).enforcements.where(voted: true).count
  end

  def survey_mean(survey_id)
    @survey = Survey.find(survey_id)
    @total = @survey.enforcements.count
    @survey.grade.to_f / @total
  end

  def enforcement_id(survey, person)
    survey.enforcements.find_by(person_id: person.id).id
  end
end
