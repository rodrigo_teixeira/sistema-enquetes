class Survey < ApplicationRecord
  has_many :enforcements, dependent: :delete_all
  has_many :people, through: :enforcements

  validates_presence_of :question

  def send_request_mails
    self.people.each do |person|
      @enforcement = self.enforcements.find_by(person_id: person.id)
      SurveyMailer.with(enforcement: @enforcement).send_request.deliver_later
    end

    self.closed = true
    self.save
  end

  def send_result_mails
    self.people.each do |person|
      @enforcement = self.enforcements.find_by(person_id: person.id)
      SurveyMailer.with(enforcement: @enforcement).send_result.deliver_later
    end
  end
end
