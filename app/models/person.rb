class Person < ApplicationRecord
  has_many :enforcements, dependent: :restrict_with_error
  has_many :surveys, through: :enforcements

  validates :email, presence: true, 'valid_email_2/email': { mx: true }, uniqueness: true
  validates :name, presence: true

  def to_s
    self.email
  end
end
