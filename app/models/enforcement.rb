class Enforcement < ApplicationRecord
  belongs_to :survey
  belongs_to :person

  validates :survey, uniqueness: { scope: :person }
  validates :person, uniqueness: { scope: :survey }

  after_save do
    if self.survey.enforcements.all? { |enforcement| enforcement.voted }
      self.survey.send_result_mails
    end
  end

  before_create do
    begin
      self.token = SecureRandom.hex[0, 10].upcase
    rescue ActiveRecord::RecordNotUnique
      retry
    end
  end
end
