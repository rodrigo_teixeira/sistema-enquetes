// Place all the behaviors and hooks related to the matching controller here. // All this logic will automatically be available in application.js.
$(document).on("turbolinks:load", function() {
  $("#people-select-new").select2({
    placeholder: "Adicione uma pessoa à pesquisa",
    allowClear: true,
    theme: "bootstrap",
    ajax: {
      url: "/people.json",
      dataType: "json",
      processResults: function(data) {
        // Tranforms the top-level key of the response object from 'items' to 'results'
        return {
          results: $.map(data, function(person, i) {
            return { id: person.id, text: person.email };
          })
        };
      }
    },
    language: "pt-BR"
  });

  $("#people-select-new").on("select2:select", function(e) {
    var data = e.params.data;

    $("#survey_people").append(
      $("<option>", {
        value: data["id"],
        text: data.text,
        selected: true
      })
    );
  });

  const peopleSelect = $("#people-select");
  const peopleFeedback = $("#select-feedback");
  const peopleList = $("#people-edit-list");
  const peopleLoading = $("#people-edit-loading");

  peopleLoading.hide();

  peopleSelect.select2({
    placeholder: "Adicione uma pessoa à pesquisa",
    allowClear: true,
    theme: "bootstrap",
    ajax: {
      url: "/people.json",
      dataType: "json",
      processResults: function(data) {
        // Tranforms the top-level key of the response object from 'items' to 'results'
        return {
          results: $.map(data, function(person, i) {
            return { id: person.id, text: person.email };
          })
        };
      }
    },
    language: "pt-BR"
  });

  peopleSelect.on("select2:unselect", function(e) {
    peopleFeedback.text("");
    peopleSelect.removeClass("is-invalid");
  });

  function setLoading() {
    peopleList.hide();
    peopleLoading.show();
  }

  function removeLoading() {
    peopleList.show();
    peopleLoading.hide();
  }

  peopleSelect.on("select2:select", function(e) {
    const data = e.params.data;
    setLoading();
    $.ajax({
      type: "POST",
      url: "add_person",
      data: {
        authenticity_token: $('[name="csrf-token"]')[0].content,
        person_id: data["id"]
      },
      success: function(response) {
        peopleList.append(
          `\
          <li class="list-group-item d-flex justify-content-between align-items-center">\
            ${response.person.email}
            <span class="badge badge-primary badge-pill clickable delete-person" enforcement_id="${
              response.enforcement_id
            }">excluir</span>\
          </li>`
        );

        removeLoading();
      },
      error: function(error) {
        peopleFeedback.text(error.responseJSON.email[0]);
        peopleSelect.addClass("is-invalid");

        removeLoading();
      },
      dataType: "json"
    });
  });

  $("#person-item").on("click", ".delete-person", function(e) {
    const event = e;
    id = e.target.attributes.enforcement_id.value;

    setLoading();

    $.ajax({
      type: "DELETE",
      url: "/enforcements/" + id + "/delete",
      data: {
        authenticity_token: $('[name="csrf-token"]')[0].content
      },
      success: function(response) {
        event.target.parentElement.remove();
        removeLoading();
      },
      error: function(error) {
        removeLoading();
        console.log(error.responseJSON);
      },
      dataType: "json"
    });
  });
});
