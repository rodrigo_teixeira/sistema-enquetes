require "rails_helper"

RSpec.describe SurveyMailer, type: :mailer do
  describe "send_request" do

    it "renders the headers" do
      enforcement = create(:enforcement)
      mail = SurveyMailer.with(enforcement: enforcement).send_request

      expect(mail.subject).to eq("Pesquisa de satisfação HappyHel")
      expect(mail.to).to eq([enforcement.person.email])
      expect(mail.from).to eq(["rh@happyel.com.br"])
    end
  end

  describe "send_result" do

    it "renders the headers" do
      enforcement = create(:enforcement)
      mail = SurveyMailer.with(enforcement: enforcement).send_request

      expect(mail.subject).to eq("Pesquisa de satisfação HappyHel")
      expect(mail.to).to eq([enforcement.person.email])
      expect(mail.from).to eq(["rh@happyel.com.br"])
    end
  end



end
