# Preview all emails at http://localhost:3000/rails/mailers/survey
class SurveyPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/survey/send
  def send_request
    SurveyMailer.send_request
  end
  
  def send_result
    SurveyMailer.send_result
  end

end
