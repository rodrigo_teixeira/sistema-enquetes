require "rails_helper"

RSpec.describe EnforcementsController, type: :routing do
  describe "routing" do
    it "routes to #edit" do
      expect(:get => "/enforcements/edit?token=AAAAAAAAA").to route_to("enforcements#edit", :token => "AAAAAAAAA")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/enforcements/1/update").to route_to("enforcements#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/enforcements/1/delete").to route_to("enforcements#destroy", :id => "1")
    end
  end
end
