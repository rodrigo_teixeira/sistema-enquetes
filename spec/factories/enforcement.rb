FactoryBot.define do
  factory:person do
    email {Faker::Internet.email }
    name {Faker::Name.name }
  end
    
  factory:survey do
    question {Faker::Lorem.sentence }
  end
    
  factory:enforcement do
    person
    survey
  end
  
end 