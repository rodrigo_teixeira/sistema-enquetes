require 'rails_helper'

RSpec.describe Survey, type: :model do
  it "is not valid without a question" do
    survey = Survey.new(question: nil)
    expect(survey).to_not be_valid
  end

  describe 'associations' do
    it { should have_many(:people).through(:enforcements) }
  end

  describe 'validations' do
    it { should validate_presence_of(:question) }
  end
end
