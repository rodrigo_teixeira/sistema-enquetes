require 'rails_helper'

RSpec.describe Person, type: :model do
  it "is not valid without a name" do
    person = Person.new(name: nil)
    expect(person).to_not be_valid
  end

  it "is not valid without a email" do
    person = Person.new(email: nil)
    expect(person).to_not be_valid
  end

  it "is not valid with a invalid email" do
    person = Person.new(email: "teste.aaaaa")
    expect(person).to_not be_valid
  end

  describe 'associations' do
    it { should have_many(:surveys).through(:enforcements) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
  end
end
