require 'rails_helper'

RSpec.describe Enforcement, type: :model do
  describe 'associations' do
    it { should belong_to(:person) }

    it { should belong_to(:survey) }
  end
  
  it "create token to enforcement" do
    person  = Person.create!(email: "person@email.com", name: "Person")
    survey = Survey.create!(question: "Teste 1")
    enforcement = Enforcement.create!(survey_id: survey.id, person_id: person.id)
    expect(enforcement.token).not_to be_nil
  end
end
