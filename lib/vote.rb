class Vote
  include ActiveModel::Validations

  attr_accessor :grade

  def initialize(args = {})
    @grade = args.fetch(:grade, nil)
  end

  validates_numericality_of :grade, greater_than_or_equal_to: 1, less_than_or_equal_to: 10
  validates_presence_of :grade
end
