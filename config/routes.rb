Rails.application.routes.draw do
  devise_for :admins, skip: [:registration]
  resources :people
  resources :surveys do
    post "add_person", on: :member
    post "send_mail", on: :member
  end

  get "enforcements/edit", to: "enforcements#edit"
  get "enforcements/denied", to: "enforcements#denied"
  patch "enforcements/:id/update", to: "enforcements#update", as: :enforcement
  delete "enforcements/:id/delete", to: "enforcements#destroy", as: :enforcement_destroy

  root "surveys#index"
end
