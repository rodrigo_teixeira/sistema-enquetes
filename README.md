# README
Things you may want to cover:

* Ruby version 2.6.3

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, database, search engines, etc.)
  - Sidekiq
  - Redis
  - PostgreSQL

* Acesso ao sistema
  - usuario: admin@happynel.com.br
  - senha: happynel123
