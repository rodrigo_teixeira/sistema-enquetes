class CreateEnforcements < ActiveRecord::Migration[5.2]
  def change
    create_table :enforcements do |t|
      t.references :survey, foreign_key: true
      t.references :person, foreign_key: true
      t.boolean :voted, default: false
      t.string :token

      t.timestamps
    end

    add_index :enforcements, [:survey_id, :person_id], unique: true
  end
end
