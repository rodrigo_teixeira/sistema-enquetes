class CreateSurveys < ActiveRecord::Migration[5.2]
  def change
    create_table :surveys do |t|
      t.decimal :grade
      t.text :question, null: false
      t.boolean :closed

      t.timestamps
    end
  end
end
