class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :email, null: false
      t.string :name, null: false

      t.timestamps
    end

    add_index :people, :email, unique: true
  end
end
